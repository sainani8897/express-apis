var express = require('express');
var router = express.Router();
var controller = require('../controllers/index')

/* GET home page. */
// router.get('/', function(req, res, next) {
//     res.render('index', { title: 'Sample' });
//   });
router.get('/',controller.index);
router.post('/',controller.store);
  
module.exports = router;